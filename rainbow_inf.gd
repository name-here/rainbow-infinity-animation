extends TextureRect

var offset := 0.94
var frame := 0
var mask:Image
@onready var sub_viewport = get_parent()

var running := true

var output_frames := false

func _ready():
	pass

func _process(_delta):
	if frame > 0:
		if output_frames:
			if frame <= 250:
				# save frames outside the project folder so Godot doesn't import them
				sub_viewport.get_texture().get_image().save_png(
					"../rainbow_inf_frames/frame_"+str(frame)+".png")
				material.set_shader_parameter("gradient_offset", offset)
				offset += 0.004
			else:
				get_tree().quit()
		elif running:
			material.set_shader_parameter("gradient_offset", offset)
			offset += 0.001
	else:
		offset += 0.004
	frame += 1
	

func _input(_event):
	if Input.is_action_just_pressed("play+pause"):
		running = !running
